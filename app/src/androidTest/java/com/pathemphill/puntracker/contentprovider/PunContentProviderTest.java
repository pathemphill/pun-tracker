package com.pathemphill.puntracker.contentprovider;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.test.InstrumentationRegistry;
import android.test.ProviderTestCase2;

public class PunContentProviderTest extends ProviderTestCase2<PunContentProvider>
{
    private ContentValues singleValidInsertValues;

    public PunContentProviderTest()
    {
        super(PunContentProvider.class, PunContract.AUTHORITY);
    }

    @Override
    public void setUp() throws Exception
    {
        setContext(InstrumentationRegistry.getTargetContext());
        super.setUp();
        singleValidInsertValues = new ContentValues();
        singleValidInsertValues.put(PunContract.PunEntry.PUN_AUTHOR, "John Smith");
        singleValidInsertValues.put(PunContract.PunEntry.PUN_TEXT, "Not very punny");
    }

    @Override
    protected void tearDown() throws Exception
    {
        super.tearDown();
        clearContentProvider();
    }

    public void testInsert() throws Exception
    {
        Uri expectedUri = Uri.withAppendedPath(PunContract.CONTENT_URI, "1");

        Uri uri = getContext().getContentResolver().insert(PunContract.CONTENT_URI, singleValidInsertValues);
        assertEquals("Uri matches", expectedUri, uri);
    }

    public void testInsertWrongUri() throws Exception
    {
        try
        {
            Uri wrongUri = Uri.parse("content://com.pathemphill.puntracker.provider/wrong");
            getContext().getContentResolver().insert(wrongUri, singleValidInsertValues);
            fail("Should have thrown Illegal Argument Exception");
        }
        catch (IllegalArgumentException iae)
        {
            // Success
        }
    }

    public void testInsertAndQuerySingleDataValid()
    {
        Uri uri = getContext().getContentResolver().insert(PunContract.CONTENT_URI, singleValidInsertValues);
        Cursor c = getContext().getContentResolver().query(uri, null, null, null, null);
        assertNotNull("cursor exists", c);
        assertEquals("Cursor retrieved correct number of records", 1, c.getCount());
        assertEquals("Cursor retrieved correct number of columns", 3, c.getColumnCount());
        assertTrue("Cursor navigates to first record", c.moveToFirst());
        assertEquals("Record id matches", 1L, c.getLong(c.getColumnIndex(PunContract.PunEntry._ID)));
        assertEquals("Author name matches", "John Smith", c.getString(c.getColumnIndex(PunContract.PunEntry.PUN_AUTHOR)));
        assertEquals("Pun text matches", "Not very punny", c.getString(c.getColumnIndex(PunContract.PunEntry.PUN_TEXT)));
        c.close();
    }

    public void testQueryCount()
    {
        insertManyRecords();
        Cursor cursor = getContext().getContentResolver().query(PunContract.CONTENT_URI, null, null, null, null);
        assertNotNull("cursor exists", cursor);
        assertEquals("Cursor has correct number of records", 5, cursor.getCount());
        assertEquals("Cursor has correct number of columns", 3, cursor.getColumnCount());
        cursor.close();
    }

    public void testQuerySelectionArgs()
    {
        insertManyRecords();
        String selection = PunContract.PunEntry.PUN_AUTHOR + " = ?";
        String [] selectionArgs = {"John Smith"};
        Cursor cursor = getContext().getContentResolver().query(PunContract.CONTENT_URI, null, selection, selectionArgs, null);
        assertNotNull("cursor exists", cursor);
        assertEquals("Cursor has correct number of records", 3, cursor.getCount());
        assertEquals("Cursor has correct number of columns", 3, cursor.getColumnCount());
        cursor.close();
    }

    public void testQueryProjection()
    {
        insertManyRecords();
        String [] projection = {PunContract.PunEntry.PUN_AUTHOR, PunContract.PunEntry.PUN_TEXT};
        Cursor cursor = getContext().getContentResolver().query(PunContract.CONTENT_URI, projection, null, null, null);
        assertNotNull("cursor exists", cursor);
        assertEquals("Cursor has correct number of records", 5, cursor.getCount());
        assertEquals("Cursor has correct number of columns", 2, cursor.getColumnCount());
        assertEquals("Cursor has correct column first", PunContract.PunEntry.PUN_AUTHOR, cursor.getColumnName(0));
        assertEquals("Cursor has correct column second", PunContract.PunEntry.PUN_TEXT, cursor.getColumnName(1));
        cursor.close();
    }

    public void testQueryOrderBy()
    {
        insertManyRecords();
        String orderBy = PunContract.PunEntry.PUN_TEXT + " ASC";
        Cursor cursor = getContext().getContentResolver().query(PunContract.CONTENT_URI, null, null, null, orderBy);
        assertNotNull("cursor exists", cursor);
        assertEquals("Cursor has correct number of records", 5, cursor.getCount());
        assertEquals("Cursor has correct number of columns", 3, cursor.getColumnCount());
        assertTrue(cursor.moveToFirst());
        assertEquals("Author name matches", "Jill Baker", cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_AUTHOR)));
        assertEquals("Pun text matches", "Cherries are berry tasty", cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_TEXT)));
        assertTrue(cursor.moveToNext());
        assertEquals("Author name matches", "John Smith", cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_AUTHOR)));
        assertEquals("Pun text matches", "Not very punny", cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_TEXT)));
        assertTrue(cursor.moveToNext());
        assertEquals("Author name matches", "Susie Queue", cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_AUTHOR)));
        assertEquals("Pun text matches", "Rhino just what to do", cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_TEXT)));
        assertTrue(cursor.moveToNext());
        assertEquals("Author name matches", "John Smith", cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_AUTHOR)));
        assertEquals("Pun text matches", "Smokey can't bear forest fires", cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_TEXT)));
        assertTrue(cursor.moveToNext());
        assertEquals("Author name matches", "John Smith", cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_AUTHOR)));
        assertEquals("Pun text matches", "The pie's the limit", cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_TEXT)));
        cursor.close();
    }

    public void testQueryNoResults()
    {
        Cursor cursor = getContext().getContentResolver().query(PunContract.CONTENT_URI, null, null, null, null);
        assertNotNull("cursor exists", cursor);
        assertEquals("Cursor found no records", 0, cursor.getCount());
        cursor.close();
    }

    public void testQueryWrongUri()
    {
        try
        {
            Uri wrongUri = Uri.parse("content://com.pathemphill.puntracker.provider/wrong");
            getContext().getContentResolver().query(wrongUri, null, null, null, null);
            fail("Should have thrown Illegal Argument Exception");
        }
        catch (IllegalArgumentException iae)
        {
            // Success
        }
    }

    public void testDeleteAll()
    {
        insertManyRecords();
        assertEquals("Deletes correct number of records", 5,
                getContext().getContentResolver().delete(PunContract.CONTENT_URI, null, null));
    }

    public void testDeleteOne()
    {
        getContext().getContentResolver().insert(PunContract.CONTENT_URI, singleValidInsertValues);
        assertEquals("Deletes correct number of records", 1,
                getContext().getContentResolver().delete(PunContract.CONTENT_URI, null, null));
    }

    public void testDeleteMany()
    {
        insertManyRecords();
        String selection = PunContract.PunEntry.PUN_AUTHOR + " = ?";
        String [] args = {"John Smith"};
        int count = getContext().getContentResolver().delete(PunContract.CONTENT_URI, selection, args);
        assertEquals("Deletes correct number of records", 3, count);
    }

    public void testDeleteNone()
    {
        insertManyRecords();
        String selection = PunContract.PunEntry.PUN_TEXT + " = ?";
        String [] args = {"No matching puns!"};
        assertEquals("Deletes correct number of records", 0,
                getContext().getContentResolver().delete(PunContract.CONTENT_URI, selection, args));
    }

    public void testDeleteWrongUri()
    {
        try
        {
            Uri wrongUri = Uri.parse("content://com.pathemphill.puntracker.provider/wrong");
            getContext().getContentResolver().delete(wrongUri, null, null);
            fail("Should have thrown Illegal Argument Exception");
        }
        catch (IllegalArgumentException iae)
        {
            // Success
        }
    }

    public void testGetTypeSingle()
    {
        Uri uri = PunContract.CONTENT_URI.buildUpon().appendPath("1").build();
        String type = getContext().getContentResolver().getType(uri);
        assertEquals("Type string is correct", "vnd.android.cursor.item/vnd.pathemphill.puns", type);
    }

    public void testGetTypeMany()
    {
        String type = getContext().getContentResolver().getType(PunContract.CONTENT_URI);
        assertEquals("Type string is correct", "vnd.android.cursor.dir/vnd.pathemphill.puns", type);
    }

    public void testGetTypeWrongUri()
    {
        Uri wrongUri = Uri.parse("content://com.pathemphill.puntracker.provider/wrong");
        String type = getContext().getContentResolver().getType(wrongUri);
        assertNull("No type returned", type);
    }

    public void testUpdateSingle()
    {
        String author = "James Bond";
        String text = "Am I chicken little or is this SkyFall?";
        insertManyRecords();
        Uri itemUri = PunContract.CONTENT_URI.buildUpon().appendPath("2").build();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PunContract.PunEntry.PUN_AUTHOR, author);
        contentValues.put(PunContract.PunEntry.PUN_TEXT, text);
        int count = getContext().getContentResolver().update(itemUri, contentValues, null, null);
        assertEquals("Only one record updated", 1, count);
        Cursor cursor = getContext().getContentResolver().query(itemUri, null, null, null, null);
        assertNotNull("Cursor exists", cursor);
        assertEquals("Cursor has one entry", 1, cursor.getCount());
        cursor.moveToFirst();
        assertEquals("Correct author", author, cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_AUTHOR)));
        assertEquals("Correct text", text, cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_TEXT)));
        cursor.close();
    }

    public void testUpdateMany()
    {
        insertManyRecords();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PunContract.PunEntry.PUN_AUTHOR, "Bubba");
        String where = PunContract.PunEntry.PUN_AUTHOR + " = ?";
        String [] selectionArgs = {"John Smith"};
        int count = getContext().getContentResolver().update(PunContract.CONTENT_URI, contentValues, where, selectionArgs);
        assertEquals("Three records updated", 3, count);

        Cursor cursor = getContext().getContentResolver().query(PunContract.CONTENT_URI, null, where, selectionArgs, null);
        assertNotNull(cursor);
        assertEquals("No records found", 0, cursor.getCount());
        cursor.close();

        selectionArgs[0] = "Bubba";
        cursor = getContext().getContentResolver().query(PunContract.CONTENT_URI, null, where, selectionArgs, null);
        assertNotNull(cursor);
        assertEquals("Three records found", 3, cursor.getCount());
        for(int i = 0; i < 3; i++)
        {
            cursor.moveToNext();
            assertEquals("Author was updated", "Bubba", cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_AUTHOR)));
        }
        cursor.close();
    }

    public void testUpdateNone()
    {
        insertManyRecords();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PunContract.PunEntry.PUN_AUTHOR, "Bubba");
        String where = PunContract.PunEntry.PUN_AUTHOR + " = ?";
        String [] selectionArgs = {"Billy"};
        int count = getContext().getContentResolver().update(PunContract.CONTENT_URI, contentValues, where, selectionArgs);
        assertEquals("No records updated", 0, count);
    }

    public void testUpdateWrongUri()
    {
        try
        {
            Uri wrongUri = Uri.parse("content://com.pathemphill.puntracker.provider/wrong");
            getContext().getContentResolver().update(wrongUri, singleValidInsertValues, null, null);
            fail("Should have thrown Illegal Argument Exception");
        }
        catch (IllegalArgumentException iae)
        {
            // Success
        }
    }

    private void clearContentProvider()
    {
        getContext().getContentResolver().delete(PunContract.CONTENT_URI, null, null);
    }

    private void insertManyRecords()
    {
        ContentValues [] valuesArray = new ContentValues[5];

        ContentValues values = new ContentValues();
        values.put(PunContract.PunEntry.PUN_AUTHOR, "John Smith");
        values.put(PunContract.PunEntry.PUN_TEXT, "Not very punny");
        valuesArray[0] = values;

        values = new ContentValues();
        values.put(PunContract.PunEntry.PUN_AUTHOR, "Jill Baker");
        values.put(PunContract.PunEntry.PUN_TEXT, "Cherries are berry tasty");
        valuesArray[1] = values;

        values = new ContentValues();
        values.put(PunContract.PunEntry.PUN_AUTHOR, "John Smith");
        values.put(PunContract.PunEntry.PUN_TEXT, "Smokey can't bear forest fires");
        valuesArray[2] = values;

        values = new ContentValues();
        values.put(PunContract.PunEntry.PUN_AUTHOR, "Susie Queue");
        values.put(PunContract.PunEntry.PUN_TEXT, "Rhino just what to do");
        valuesArray[3] = values;

        values = new ContentValues();
        values.put(PunContract.PunEntry.PUN_AUTHOR, "John Smith");
        values.put(PunContract.PunEntry.PUN_TEXT, "The pie's the limit");
        valuesArray[4] = values;

        getContext().getContentResolver().bulkInsert(PunContract.CONTENT_URI, valuesArray);
    }
}

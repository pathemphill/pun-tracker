package com.pathemphill.puntracker.adapters;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pathemphill.puntracker.R;
import com.pathemphill.puntracker.contentprovider.PunContract;

public class PunRecyclerCursorAdapter extends RecyclerView.Adapter<PunRecyclerCursorAdapter.ViewHolder>
{
    private Cursor cursor;
    private OnPunClickedListener listener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewHolder holder = new ViewHolder(inflater.inflate(R.layout.pun_view_holder, parent, false));

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        if (cursor != null && position < cursor.getCount())
        {
            cursor.moveToPosition(position);
            holder.authorText.setText(
                    cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_AUTHOR))
            );
            holder.punText.setText(
                    cursor.getString(cursor.getColumnIndex(PunContract.PunEntry.PUN_TEXT))
            );
            holder.holderLayout.setClickable(true);

            final int punId = cursor.getInt(cursor.getColumnIndex(PunContract.PunEntry._ID));
            holder.holderLayout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (listener != null)
                    {
                        listener.onPunClicked(punId);
                    }
                }
            });
        }
        else
        {
            holder.authorText.setText(holder.authorText.getContext().getString(R.string.no_author));
            holder.punText.setText(holder.punText.getContext().getString(R.string.no_pun));
        }
    }

    @Override
    public int getItemCount()
    {
        return (cursor == null ? 0: cursor.getCount());
    }

    public void changeCursor(Cursor cursor)
    {
        if (this.cursor != null)
        {
            this.cursor.close();
        }
        this.cursor = cursor;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public RelativeLayout holderLayout;
        public TextView authorText;
        public TextView punText;

        public ViewHolder(View v)
        {
            super(v);
            holderLayout = (RelativeLayout) v.findViewById(R.id.pun_view_holder_layout);
            authorText = (TextView) v.findViewById(R.id.author_text);
            punText = (TextView) v.findViewById(R.id.pun_text);
        }
    }

    public void setListener(OnPunClickedListener listener)
    {
        this.listener = listener;
    }

    public interface OnPunClickedListener
    {
        void onPunClicked(int punId);
    }
}

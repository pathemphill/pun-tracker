package com.pathemphill.puntracker.activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.pathemphill.puntracker.R;
import com.pathemphill.puntracker.adapters.PunRecyclerCursorAdapter;
import com.pathemphill.puntracker.contentprovider.PunContract;

public class PunListActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor>,
        PunRecyclerCursorAdapter.OnPunClickedListener
{
    private RecyclerView recyclerView;
    private TextView emptyTextView;

    private PunRecyclerCursorAdapter adapter;

    //identify the pun loader. Most useful when multiple loaders would be in use.
    private static final int PUN_LOADER = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pun_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchCreatorActivity();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.pun_recycler);
        adapter = new PunRecyclerCursorAdapter();
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        emptyTextView = (TextView) findViewById(R.id.empty_text);

        toggleEmptyDisplay(true);
        getSupportLoaderManager().initLoader(PUN_LOADER, null, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pun_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void toggleEmptyDisplay(boolean isEmpty)
    {
        if (isEmpty)
        {
            emptyTextView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        else
        {
            emptyTextView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void launchCreatorActivity()
    {
        Intent startIntent = new Intent(this, EditPunActivity.class);
        startActivity(startIntent);
    }

    private void launchEditorActivity(int punId)
    {
        Intent startIntent = new Intent(this, EditPunActivity.class);

        Bundle args = new Bundle();
        args.putInt(EditPunActivity.BUNDLE_KEY_PUN_ID, punId);
        startIntent.putExtras(args);

        startActivity(startIntent);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        switch (id)
        {
            case PUN_LOADER:
                String [] projection = {PunContract.PunEntry._ID, PunContract.PunEntry.PUN_AUTHOR, PunContract.PunEntry.PUN_TEXT};
                String sortBy = PunContract.PunEntry.PUN_AUTHOR + " ASC"; //sort puns by author alphabetically
                return new CursorLoader(this,
                        PunContract.CONTENT_URI,
                        projection,
                        null, //select all puns
                        null, //no selection args
                        sortBy);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        if (data != null && data.getCount() > 0)
        {
            adapter.changeCursor(data);
            toggleEmptyDisplay(false);
        }
        else
        {
            toggleEmptyDisplay(true);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        //clear out cursor before loading new one
        adapter.changeCursor(null);
    }

    @Override
    public void onPunClicked(int punId)
    {
        launchEditorActivity(punId);
    }
}

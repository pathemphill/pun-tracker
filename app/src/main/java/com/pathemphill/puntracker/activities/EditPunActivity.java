package com.pathemphill.puntracker.activities;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.pathemphill.puntracker.R;
import com.pathemphill.puntracker.contentprovider.PunContract;

public class EditPunActivity extends AppCompatActivity
{
    public static final String BUNDLE_KEY_PUN_ID = "com.pathemphill.puntracker.bundle.keys.pun.id";
    private int punId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pun);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        punId = getIntent().getIntExtra(BUNDLE_KEY_PUN_ID, -1);

        //If it isn't a new pun
        if (punId != -1)
        {
            EditText authorText = (EditText) findViewById(R.id.author_edit_text);
            EditText punText = (EditText) findViewById(R.id.pun_edit_text);

            Uri punUri = Uri.withAppendedPath(PunContract.CONTENT_URI, Integer.toString(punId));
            String [] args = {Integer.toString(punId)};
            Cursor c = getContentResolver().query(punUri, null, PunContract.PunEntry._ID + " = ?", args, null);
            c.moveToFirst();
            authorText.setText(c.getString(c.getColumnIndex(PunContract.PunEntry.PUN_AUTHOR)));
            punText.setText(c.getString(c.getColumnIndex(PunContract.PunEntry.PUN_TEXT)));
            c.close();
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                savePun();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void savePun()
    {
        EditText authorText = (EditText) findViewById(R.id.author_edit_text);
        EditText punText = (EditText) findViewById(R.id.pun_edit_text);

        ContentValues values = new ContentValues();
        values.put(PunContract.PunEntry.PUN_AUTHOR, authorText.getText().toString());
        values.put(PunContract.PunEntry.PUN_TEXT, punText.getText().toString());
        Snackbar savedSnackbar = Snackbar.make(authorText, R.string.pun_saved, Snackbar.LENGTH_SHORT);
        Snackbar errorSnackbar = Snackbar.make(authorText, R.string.pun_save_fail, Snackbar.LENGTH_SHORT);

        //Do a new insert
        if (punId == -1)
        {
            Uri uri = getContentResolver().insert(PunContract.CONTENT_URI, values);
            if (uri != null)
            {
                //update pun id
                String[] projection = {PunContract.PunEntry._ID};
                Cursor c = getContentResolver().query(uri, projection, null, null, null);
                c.moveToFirst();
                punId = c.getInt(c.getColumnIndex(PunContract.PunEntry._ID));
                getIntent().putExtra(BUNDLE_KEY_PUN_ID, punId);
                c.close();
                savedSnackbar.show();
            }
            else
            {
                errorSnackbar.show();
            }
        }
        //Update row in Database
        else
        {
            Uri punUri = Uri.withAppendedPath(PunContract.CONTENT_URI, Integer.toString(punId));
            int updateCount = getContentResolver().update(punUri, values, null, null);
            if (updateCount == 1)
            {
                savedSnackbar.show();
            }
            else
            {
                errorSnackbar.show();
            }
        }

    }
}

package com.pathemphill.puntracker.contentprovider;

import android.net.Uri;
import android.provider.BaseColumns;

public class PunContract
{
    public static final String AUTHORITY = "com.pathemphill.puntracker.provider";
    public static final String TABLE = "pun";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE);

    public static final String DELETE_TABLE_COMMAND =
            "DROP TABLE IF EXISTS " + TABLE;
    public static final String CREATE_TABLE_COMMAND =
            "CREATE TABLE " + TABLE + " (" +
                    BaseColumns._ID + " INTEGER PRIMARY KEY," +
                    PunEntry.PUN_AUTHOR + " TEXT," +
                    PunEntry.PUN_TEXT + " TEXT" +
                    ")";

    public PunContract() {}

    public static abstract class PunEntry implements BaseColumns
    {
        public static final String PUN_AUTHOR = "author";
        public static final String PUN_TEXT = "pun_text";

    }
}

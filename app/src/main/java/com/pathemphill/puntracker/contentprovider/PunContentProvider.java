package com.pathemphill.puntracker.contentprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

public class PunContentProvider extends ContentProvider
{
    private PunDBHelper dbHelper;

    private static final int PUNS = 1;
    private static final int PUN_ID = 2;

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static
    {
        uriMatcher.addURI(PunContract.AUTHORITY, PunContract.TABLE, PUNS);
        uriMatcher.addURI(PunContract.AUTHORITY, PunContract.TABLE + "/#", PUN_ID);
    }

    @Override
    public boolean onCreate()
    {
        dbHelper = new PunDBHelper(getContext());

        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(PunContract.TABLE);

        switch (uriMatcher.match(uri))
        {
            case PUNS:
                break;
            case PUN_ID:
                qb.appendWhere( PunContract.PunEntry._ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        Cursor c = qb.query(db, projection, selection, selectionArgs,null, null, sortOrder);

        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Nullable
    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case PUNS:
                return "vnd.android.cursor.dir/vnd.pathemphill.puns";
            case PUN_ID:
                return "vnd.android.cursor.item/vnd.pathemphill.puns";
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        if (uriMatcher.match(uri) == PUNS)
        {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            long rowID = db.insert(PunContract.TABLE, "", values);

            if (rowID > 0)
            {
                Uri result = ContentUris.withAppendedId(PunContract.CONTENT_URI, rowID);
                getContext().getContentResolver().notifyChange(result, null);
                return result;
            }
            throw new SQLException("Failed to add a record into " + uri.toString());
        }
        else
        {
            throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int count = 0;

        switch (uriMatcher.match(uri))
        {
            case PUNS:
                count = db.delete(PunContract.TABLE, selection, selectionArgs);
                break;
            case PUN_ID:
                count = db.delete(PunContract.TABLE, PunContract.PunEntry._ID + " = " + uri.getLastPathSegment() +
                        ((selection != null && !selection.isEmpty()) ? " AND (" + selection + ')': ""),
                        selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int count = 0;

        switch (uriMatcher.match(uri))
        {
            case PUNS:
                count = db.update(PunContract.TABLE, values, selection, selectionArgs);
                break;
            case PUN_ID:
                count = db.update(PunContract.TABLE, values, PunContract.PunEntry._ID + " = " + uri.getLastPathSegment() +
                                ((selection != null && !selection.isEmpty()) ? " AND (" + selection + ')': ""),
                        selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return count;
    }
}
